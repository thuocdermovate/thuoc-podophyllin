Thuốc đặc chữa trị căn bệnh vảy nến á sừng, viêm da, eczema (chàm), dưỡng da dermovate
Căn bệnh vẩy nến được nhận ra từ thời thượng cổ và là một trong một số căn bệnh da vô cùng thường xảy ra ở Việt Nam cũng như khá nhiều nước trên thế giới. Theo thống kê, [thuốc dermovate cream](http://phongkhamdaidong.vn/mua-thuoc-dermovate-cream-cua-thai-lan-o-dau-het-bao-nhieu-tien-753.html) tỷ lệ căn bệnh vẩy nến không giống nhau dựa từng ở vùng, từng châu lục, xong nó dao động trong khoảng 1-3% dân số.
1. Triệu chứng của căn bệnh vẩy nến như thế nào ?
Thương tổn da: hay gặp và cơ bản nhất là một số dát đỏ có vẩy trắng phủ trên bề mặt, vẩy dày, có khá nhiều lớp xếp chồng lên nhau rất dễ bong cũng như giống như giọt nến (Vì vậy có tên gọi là “Vẩy nến”). Kích thước thương tổn to nhỏ không giống nhau với đường kính từ 1- 20 cm hay lớn hơn.
Vị trí chủ yếu nhất của những dát đỏ có vẩy là da đầu, tại vùng tì đè, hay cọ xát như: khuỷu tay, đầu gối, rìa tóc, vùng xương cùng, mông, bộ phận sinh dục… tuy nhiên sau một thời gian tiến triển các thương tổn có thể lan ra toàn thân.
Thương tổn móng: Có khoảng 30% – 40% người bệnh vẩy nến bị tổn thương móng tay, móng chân. Các móng ngả màu vàng đục, có một số chấm lỗ rỗ trên bề mặt. Có khả năng móng dày, dễ mủn hoặc mất cả móng.
 
căn bệnh vảy nến á sừng
Thương tổn khớp: Tỷ lệ khớp mắc thương tổn trong vẩy nến căn cứ từng thể. Thể nhẹ, thương tổn da khu trú, chỉ có khoảng 2% bệnh nhân có biểu hiện khớp. Trong khi đấy ở thể nặng, dai dẳng có tới 20% phái mạnh có thương tổn khớp. Biểu hiện hay thấy nhất là viêm khớp mạn tính, biến dạng khớp, cứng khớp, lệch khớp, đấng mày râu cử động di chuyển khá phức tạp … một số người bệnh thương tổn da rất ít nhưng dấu hiệu ở khớp vô cùng nặng, đặc biệt là khớp gối cũng như cột sống.
người bị mắc bệnh cảm thấy ngứa ít hay rất nhiều, thường ngứa rất nhiều ở thời kỳ đang tiến triển. Những người bệnh không ngứa mà chỉ có cảm giác vướng víu, ảnh hưởng đến thẩm mỹ. Có thể gặp tổn thương móng, bản móng có hố lõm nhỏ, hoặc có những con đường kẻ theo chiều dọc. Có khi móng dòn vụn, dày ở bờ, 10 móng cùng bị một khi dùng [thuốc podophyllin](http://phongkhamdaidong.vn/mua-thuoc-podophyllin-25-chua-benh-sui-mao-ga-o-dau-696.html).
Vảy nến ở da đầu thường là một số đám mảng đỏ, nền cộm, bề mặt phủ vảy trắng, thường mọc lấn ra trán thành một viền gọi là vành vảy nến. Tóc vẫn mọc xuyên thông qua tổn thương. Tại vùng sau tai đỏ, có vết nứt, có khi xuất tiết, dễ nhầm với viêm da đầu…
căn bệnh tiến triển mãn tính, một số đợt cao trào xen kẽ một số đợt thuyên giảm. Vảy nến lành tính, người bệnh sống khỏe mạnh suốt đời, trừ những thể nặng như vảy nến thể khớp, vảy nến đỏ da toàn thân.
2.Nguyên nhân:
Mặc dù cho đến nay vẫn chưa khẳng định rõ ràng lý do. Tuy nhiên người ta biết chắc chắn 5 yếu tố Sau đây khiến bắt buộc cơ chế sinh bệnh:
– Di truyền: Khoảng 30% đấng mày râu có yếu tố gia đình (cha, mẹ, anh chị em ruột hay họ hàng trực hệ); 70% một số cặp song sinh cùng mắc. Một số nghiên cứu chỉ ra một số kháng nguyên HLAW6, B13, B17, DR7 liên quan tới vẩy nến da cũng như khớp.
– Nhiễm khuẩn: Vẩy nến ở trẻ em, vẩy nến thể giọt người ta phân lập được liên cầu khuẩn ở tổn thương cũng như điều trị kháng sinh thì bệnh thuyên giảm.
– Stress: làm cho bệnh tái phát hoặc đột ngột nặng lên.
– Thuốc: căn bệnh vẩy nến xuất hiện sau lúc dùng các thuốc: chẹn beta kéo dài, lithium, đặc biệt sau khi sử dụng corticoid.
– Hiện thượng Kobner: Thương tổn mọc lên sau những kích thích cơ học (gãi, chà xát) hoặc một số kích thích lí hóa (bệnh nặng nhẹ theo mùa). Càng mùa khô thì càng nặng.
3. Vẩy nến có bao nhiêu thể? và [thuốc dermovate cream](http://benhvienk.com/tong-hop/thuoc-dermovate-cream-cua-thai-lan-mua-o-dau-gia-bao-nhieu/)
tùy theo tính chất, đặc điểm lâm sàng, người ta chia vẩy nến khiến 2 thể chính: Thể thông thường và thể đặc biệt.
Trong thể thông thường, tùy thuộc vào kích thước, vị trí của thương tổn da, người ta phân khiến những thể như: Thể giọt, thể đồng tiền, thể mảng, vẩy nến ở đầu, vẩy nến đảo ngược, …
Thể đặc biệt ít gặp hơn tuy nhiên nặng cũng như rất khó chữa trị hơn. Đấy là những thể: Vẩy nến thể mủ, vẩy nến thể móng khớp, vẩy nến thể đỏ da toàn thân.
 
 
 
4. Căn bệnh vẩy nến cần kiêng kỵ các gì?
Tránh căng thẳng (stress)
Tránh kì cọ cũng như bóc da (hiện tượng Kobner)
Tránh để ở tại vùng da mắc căn bệnh tiếp xúc với một số chất có tính bazơ cao như xạt phòng, vôi,… vì lúc đó vùng da nhiễm căn bệnh sẽ mở rộng ra.
Cẩn thẩn khi sử dụng thuốc nếu mắc thêm những bệnh về tim mạch.
Tránh nhiễm khuẩn: Đặc biệt là nhiễm khuẩn tai, mũi, họng.
Tránh rượu: Vì rượu khiến cho căn bệnh nặng lên và tương kị với những thuốc trị.
cần lạc quan với căn bệnh tật: Dù đang bị căn bệnh nhưng bạn hãy lạc quan sẽ khỏi, không nghĩ đến nó bằng cách xem phim, khiến việc…
Tránh dẫn đến trầy xước da tại vùng này, sẽ dẫn đến nhiễm trùng, vết thương trở lên đau đớn. Cẩn thận khi sử dụng một số loại thuốc bôi dưỡng da, buộc phải bắt buộc xem kĩ những mẫu thuốc bôi dưỡng da có ảnh hưởng tới tại vùng da mắc bệnh hoặc không.
nếu như bệnh gặp ở bàn chân thì phải luôn luôn đi giầy có bít tất lúc đi ra bên ngoài đường, điều này sẽ giúp da chân không cảm thấy khô, đồng thời ngăn ngừa một số vết nứt ở da mắc nhiễm khuẩn.
